# Back-end

# Deploy

Após clonar o repositório, execute, a partir a raíz:

    docker compose up --build

Isso inicializará tanto a API quanto o banco de dados. Ao iniciar, a API automaticamente populará o MYSQL com algumas informações basicas, 
além de criar o <i>schema</i> do banco.

Além disso, um portforwarding nas portas 5000 e 3306 ocorrerão na máquina host. As portas respectivamente acessarão os containeres: API e DB. Sendo assim, é possível se conectar ao database utilizando uma IDE à sua escolha, através da porta 3306. O mesmo vale para a API.

Para desmaterializar o ambiente, basta rodar:

    docker compose down

# Funcionalidades essenciais

- [x] Abertura de tickets (cliente)
- [x] Distribuição de tickets para os desenvolvedores e preenchimento de provável data de fechamento (administrador)
- [x] Consulta de detalhamento de ticket (lista de tickets, quando clica abre um específico)
- [x] Adicionar notas ao ticket
- [x] Fechamento da nota pelo desenvolvedor