FROM python:3.10.0b2-alpine3.13

COPY ./app /osticket_api
COPY ./requirements.txt /osticket_api

RUN pip install -r /osticket_api/requirements.txt

ENTRYPOINT [ "python", "/osticket_api/main.py" ]