from model import ticketsM
from model import usersM
from controller import validateC

def fetch_ticket(data):
    has_team_permission = validateC.check_team_permission(data["token"], data["ticket_id"])
    if has_team_permission != True : return has_team_permission
    ticket = ticketsM.Ticket(data["ticket_id"])
    ticket_data = {
        "ticket_id": ticket.id,
        "owner_id": ticket.owner_id,
        "creator_id": ticket.creator_id,
        "team_id": ticket.team_id,
        "state": ticket.state,
        "priority": ticket.priority,
        "created_at": ticket.created_at,
        "title": ticket.title,
        "body": ticket.body,
        "closed_at": ticket.closed_at,
        "creator_username": ticket.creator_username,
        "owner_username": ticket.owner_username,
        "team_name": ticket.team_name,
        "valid_owners": ticket.valid_owners
    }
    try:
        ticket_data["notes"] = ticket.parsed_notes
    except:
        ticket_data["notes"] = ticket.notes
    return ticket_data

def set_owner(data):
    has_team_permission = validateC.check_team_permission(data["token"], data["ticket_id"])
    if has_team_permission != True : return has_team_permission
    ticket = ticketsM.Ticket(data["ticket_id"])
    if ticket.set_owner(data["owner_id"]) : return {"Success": "Owner set"}
    return {"Error": "Could not set owner"}

def set_state(data):
    has_team_permission = validateC.check_team_permission(token=data["token"], ticket_id=data["ticket_id"])
    if has_team_permission != True : return has_team_permission
    ticket = ticketsM.Ticket(data["ticket_id"])
    if ticket.set_state(data["state"]) : return {"Success": "State set"}
    return {"Error": "Could not set state"}

def set_priority(data):
    has_team_permission = validateC.check_team_permission(data["token"], data["ticket_id"])
    if has_team_permission != True : return has_team_permission
    ticket = ticketsM.Ticket(data["ticket_id"])
    if ticket.set_priority(data["priority"]) : return {"Success": "Priority set"}
    return {"Error": "Could not set priority"}

def create_ticket(data):
    user = usersM.User(token=data["token"])
    if user.role not in ["client", "admin"] : return {"Error": "This user cant create tickets"}
    create_result = ticketsM.create_ticket(
        creator_id=user.id,
        team_id=data["team_id"],
        state=data["state"],
        priority=data["priority"],
        title=data["title"],
        body=data["body"]
    )
    if create_result : return {"Success": "Ticket created!"}
    return {"Error": "Couldn't create ticket"}

def add_note(data):
    has_team_permission = validateC.check_team_permission(data["token"], data["ticket_id"])
    if has_team_permission != True : return has_team_permission
    ticket = ticketsM.Ticket(data["ticket_id"])
    if ticket.add_note(data["token"], data["note"]) : return {"Success": "Note added"}
    return {"Error": "Couldn't add note"}
